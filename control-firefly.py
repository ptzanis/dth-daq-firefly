import hal
from I2CInterface import I2CInterface
import time
import argparse
import sys

def main():

    parser = argparse.ArgumentParser(description='Firefly control script')

    parser.add_argument('--board', type=str, help='DTH type (P1 or P2)')

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()

    if args.board == 'P1':
        dth = hal.PCIDevice('config/DTHAddressMap_P1.dat', 0x10DC, 0x01B5, 0)
        compilation_version = dth.read("Comp_version",0 )
        i2c_switch  = I2CInterface(dth, 0xE0, offsetWidth = 0x0 , dataWidth = 0x1,
                                        items={'i2c_a': 'FireFly_i2c_access_a', 'i2c_b': 'FireFly_i2c_access_b',
                                               'i2c_poll': 'FireFly_i2c_access_done'})
        i2c_firefly = I2CInterface(dth, 0xA0, offsetWidth = 0x1 , dataWidth = 0x1,
                                        items={'i2c_a': 'FireFly_i2c_access_a', 'i2c_b': 'FireFly_i2c_access_b',
                                               'i2c_poll': 'FireFly_i2c_access_done'})
    elif args.board == 'P2':
        dth = hal.AXIDevice("axi_chip2chip@a0000000", "config/DTHAddressMap_P2.dat")
        compilation_version = dth.read("compilation_version_off",0 )
        i2c_switch  = I2CInterface(dth, 0xE0, offsetWidth = 0x0 , dataWidth = 0x1,
                                        items={'i2c_a': 'firefly_i2c_access_a', 'i2c_b': 'firefly_i2c_access_b',
                                               'i2c_poll': 'firefly_i2c_access_a_access_done'})
        i2c_firefly = I2CInterface(dth, 0xA0, offsetWidth = 0x1 , dataWidth = 0x1,
                                        items={'i2c_a': 'firefly_i2c_access_a', 'i2c_b': 'firefly_i2c_access_b',
                                               'i2c_poll': 'firefly_i2c_access_a_access_done'})
    
    print("Compilation Version:" +str(compilation_version))

    print( "Setting up Fireflies" )
    
    dth.write("reset_hw_comp",0x80000000) 
    time.sleep(0.001)
    dth.write("reset_hw_comp",0x00000000)

    #read Firefly status
    if args.board == 'P1':
        #Enable all FF
        dth.write("Firefly_monitoring",0x222222) 
        FF_status = dth.read("Firefly_monitoring",0)   
    elif args.board == 'P2':
        #Enable all FF
        dth.write("firefly_monitoring",0x222222) 
        FF_status = dth.read("firefly_monitoring",0)

    print("Firefly status : (CS(b0),Reset(b1),Present(b2),Int(b3))" + str( hex(FF_status)) )

    # switch_TCA = [0x1,0x2,0x4,0x8,0x10,0x20]
    switch_TCA = [0x8, 0x10]
    # P2 FED0 -> 0x1 0x2 
    # P2 FED1 -> 0x4 0x20 
    # P2 FED2 -> 0x8 0x10 

    # switch_TCA = [0x1,0x8] # J9 -> 0x1 , J12 -> 0x8

    if( (FF_status & 0x04) == 0x0):
        i2c_switch.write(0x0, switch_TCA[0])
        for x in range(len(switch_TCA)):
            print("------ Access Firefly #", x , ", Switch TCA : ", hex(switch_TCA[x]), "------")
            i2c_switch.write(0x0, switch_TCA[x])
            time.sleep(0.1)
            print("Firefly Temperature : ",str(i2c_firefly.read(22)) + " C")

            binary_input = input("Enter a binary value (e.g., 0b1110): ")
            i2c_firefly.write(86, int(binary_input, 2))

            print("Readback Status Tx1:", i2c_firefly.read(86) & 0x01 != 0x01)
            print("Readback Status Tx2:", i2c_firefly.read(86) & 0x02 != 0x02)
            print("Readback Status Tx3:", i2c_firefly.read(86) & 0x04 != 0x04)
            print("Readback Status Tx4:", i2c_firefly.read(86) & 0x08 != 0x08)

if __name__ == "__main__":
	main()
